﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace Sample03
{
	public class ExpressionToFTSRequestTranslator : ExpressionVisitor
	{
		StringBuilder resultString;

		public string Translate(Expression exp)
		{
			resultString = new StringBuilder();
			Visit(exp);

			return resultString.ToString();
		}

		protected override Expression VisitMethodCall(MethodCallExpression node)
		{
			if (node.Method.DeclaringType == typeof(Queryable)
				&& node.Method.Name == "Where")
			{
				var predicate = node.Arguments[1];
				Visit(predicate);

				return node;
			}

		    if (node.Method.DeclaringType == typeof (string))
		    {
		        switch (node.Method.Name)
		        {
                    case "Contains":
                        resultString.Append(string.Format("{0}:(*{1}*)",node.Object, node.Arguments[1]));
                        break;
                    case "EndsWith":
                        resultString.Append(string.Format("{0}:(*{1})", node.Object, node.Arguments[1]));
                        break;
                    case "StartsWith":
                        resultString.Append(string.Format("{0}:({1}*)", node.Object, node.Arguments[1]));
                        break;
                }
		    }

			return base.VisitMethodCall(node);
		}

        //protected override Visi

		protected override Expression VisitBinary(BinaryExpression node)
		{
			switch (node.NodeType)
			{
				case ExpressionType.Equal:
			        if (!(node.Left.NodeType == ExpressionType.MemberAccess) && !(node.Right.NodeType == ExpressionType.Constant))
			        {
                        resultString.Append("(");
                        Visit(node.Left);
                        resultString.Append(")");
                        Visit(node.Right);
                    }
			        //throw new NotSupportedException(string.Format("Left operand should be property or field", node.NodeType));

			        //throw new NotSupportedException(string.Format("Right operand should be constant", node.NodeType));
			        else if (!(node.Right.NodeType == ExpressionType.MemberAccess) && !(node.Left.NodeType == ExpressionType.Constant))
                    {
                        Visit(node.Right);
			            resultString.Append("(");
                        Visit(node.Left);
                        resultString.Append(")");
			        }

			        break;

                    case ExpressionType.AndAlso:
			        VisitMethodCall((MethodCallExpression)node.Left);
			        resultString.Append(" AND ");
                    VisitMethodCall((MethodCallExpression)node.Right);

                    break;

				default:
					throw new NotSupportedException(string.Format("Operation {0} is not supported", node.NodeType));
			};

			return node;
		}

		protected override Expression VisitMember(MemberExpression node)
		{
			resultString.Append(node.Member.Name).Append(":");

			return base.VisitMember(node);
		}

		protected override Expression VisitConstant(ConstantExpression node)
		{
			resultString.Append(node.Value);

			return node;
		}
	}
}
